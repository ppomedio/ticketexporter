��          �      <      �     �     �     �      �  $   
  *   /  (   Z     �     �  	   �     �     �     �     �     �          !  _  >     �     �     �      �  $   �  *     (   G     p     �  	   �     �     �     �     �     �     �                               
             	                                                              %1$s (%2$s) %1$s by %2$s. %1$s, opened on %2$s by %3$s. An error occurred during export. Assigned to group Assigned to groups Assigned to supplier Assigned to suppliers Associated document Associated documents Configuration check failure. Export ticket Exporting GLPI Ticket #%1$s Nobody None PDF only (no attachments) This plugin requires GLPI v9.3. Ticket Exporting Ticket export in progress… Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-15 15:57+0100
Last-Translator: Philippe Pomédio <philippe.pomedio@univ-cotedazur.fr>
Language-Team: none
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
 %1$s (%2$s) %1$s by %2$s. %1$s, opened on %2$s by %3$s. An error occurred during export. Assigned to group Assigned to groups Assigned to supplier Assigned to suppliers Associated document Associated documents Configuration check failure. Export ticket Exporting GLPI Ticket #%1$s Nobody None PDF only (no attachments) This plugin requires GLPI v9.3. Ticket Exporting Ticket export in progress… 