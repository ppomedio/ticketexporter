<?php

$_SESSION['glpi_use_mode'] = Session::DEBUG_MODE;
#Toolbox::logDebug(phpversion());


/**
 * Plugin install process.
 *
 * @return boolean
 */
function plugin_ticketexporter_install() {

    $tools = new PluginTicketexporterToolbox();

    return $tools->createWorkingFolder();
}


/**
 * Plugin uninstall process.
 *
 * @return boolean
 */
function plugin_ticketexporter_uninstall() {

    $tools = new PluginTicketexporterToolbox();
    $tools->removeWorkingFolder();

    return true;
}

// EOF
