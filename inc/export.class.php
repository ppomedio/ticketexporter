<?php

/**
 * Plugin's export class.
 *
**/
class PluginTicketexporterExport {

    private $pdf;
    private $ticket;
    private $userOptions;
    private $ticketStatus;
    private $ticketFollowup;
    private $ticketApprovals;
    private $ticketRequesterName;
    private $tableContent = '';
    private $html = '';

    public $ticketID;
    public $ticketFileName;
    public $transferFileName;
    public $ticketAttachments;


    /**
     * Class initialization.
     *
    **/
    function __construct () {

        $this->pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8');
        $this->ticket = new Ticket();

    }


    /**
     * Loads ticket data, sets TCPDF.
     *
     * @param $formData array
     *
     * @return boolean
    **/
    public function init($formData) {

        if (!$this->ticket->getFromDB($formData['id'])) {
            return false;
        };

        global $CFG_GLPI;

        $this->ticketID = $formData['id'];
        unset($formData['id']);
        $this->userOptions = $formData;
        $this->ticketFileName = sprintf(
            __('GLPI Ticket #%1$s', 'ticketexporter'), $this->ticketID);
        $this->transferFileName = sprintf(
            "GLPI-%s_%s", __("Ticket"), $this->ticketID);
        $this->ticketRequesterName = $this->getUserName(
            $this->ticket->fields['users_id_recipient']);
        $this->ticketAttachments = $this->getAttachments();
        $this->ticketApprovals = $this->getApprovals();
        $this->ticketFollowup = $this->getFollowup();

        if (in_array($this->ticket->fields['status'],
                $this->ticket->getClosedStatusArray())) {
            $eventDate = Html::convDateTime($this->ticket->fields['closedate']);
        } else if (in_array($this->ticket->fields['status'],
                $this->ticket->getSolvedStatusArray())) {
            $eventDate = Html::convDateTime($this->ticket->fields['solvedate']);
        } else if (in_array($this->ticket->fields['status'],
                $this->ticket->getReopenableStatusArray())) {
            $eventDate = Html::convDateTime(
                $this->ticket->fields['begin_waiting_date']);
        }
        if (isset($eventDate)) {
            $this->ticketStatus = sprintf(
                __('%1$s (%2$s)', 'ticketexporter'),
                Html::clean(
                    $this->ticket->getStatus($this->ticket->fields['status'])),
                $eventDate
            );
        } else {
            $this->ticketStatus = Html::clean(
                $this->ticket->getStatus($this->ticket->fields['status']));
        }

        $font = $_SESSION['glpipdffont'];

        $this->pdf->setFont($font, '', 12);
        $this->pdf->setPrintHeader(true);
        $this->pdf->setPrintFooter(true);
        $this->pdf->setHeaderFont([$font, '', 14]);
        $this->pdf->setFooterFont([$font, '', 10]);
        $this->pdf->setMargins(10, 20, 10, true);
        $this->pdf->setHeaderMargin(5);
        $this->pdf->setFooterMargin(7);
        $this->pdf->setCellHeightRatio(1.3);
        $this->pdf->setAutoPageBreak(true, 10);

        $this->pdf->setTitle($this->ticketFileName);
        $this->pdf->setSubject($this->ticket->fields['name']);
        $this->pdf->setCreator("GLPI (" . $CFG_GLPI['url_base'] . ")");
        $this->pdf->setAuthor(Html::clean($this->ticketRequesterName));

        $this->pdf->setHeaderData("", 0, $this->ticketFileName, sprintf(
            __('%1$s, opened on %2$s by %3$s.', 'ticketexporter'),
            $this->ticketStatus,
            Html::convDate($this->ticket->fields['date']),
            Html::clean($this->ticketRequesterName)
        ));

        return true;
    }


    /**
     * Creates PDF from ticket data.
     *
     * @param $pdfOnly boolean
     *
     * @return boolean
    **/
    public function createPdf($pdfOnly = false) {

        /**
         * Returns a formatted line of data for an item,
         * eventually localized by GLPI.
         *
         * @param $title       string
         * @param $text        string
         * @param $toLocalize  boolean
         *
         * @return string
        **/
        function format($title, $text, $toLocalize = true) {

            if ($toLocalize) {
                $title = __($title);
            }

            return sprintf(
                "<b>" . __('%1$s: %2$s'), $title, "</b>") . $text;
        }

        $this->buildRow(format(
            "Title", Html::clean($this->ticket->fields['name'])));

        if ($this->ticket->fields['name'] != $this->ticket->fields['content']) {
            $this->buildRow(format(
                "Description", Html::clean($this->ticket->fields['content'])));
        }

        $this->buildRow(format(
            "Entity", Html::clean(Dropdown::getDropdownName(
                "glpi_entities", $this->ticket->fields['entities_id']))));

        $this->buildRow();

        $this->buildRow(
            format("Type", Html::clean(
                Ticket::getTicketTypeName($this->ticket->fields['type']))),
            format("Category", Html::clean(
                Dropdown::getDropdownName('glpi_itilcategories',
                    $this->ticket->fields['itilcategories_id'])))
        );

        $this->buildRow(
            format("Status", $this->ticketStatus),
            format("Request source", Html::clean(
                Dropdown::getDropdownName('glpi_requesttypes',
                    $this->ticket->fields['requesttypes_id'])))
        );

        $this->buildRow(
            format("Urgency", Html::clean(
                $this->ticket->getUrgencyName($this->ticket->fields['urgency']))),
            format("Approval", TicketValidation::getStatus(
                $this->ticket->fields['global_validation']))
        );

        $this->buildRow(
            format("Impact", Html::clean(
                $this->ticket->getImpactName($this->ticket->fields['impact']))),
            format("Location", Html::clean(
                Dropdown::getDropdownName('glpi_locations',
                    $this->ticket->fields['locations_id'])))
        );

        $this->buildRow(
            format("Priority", Html::clean(
                $this->ticket->getPriorityName(
                    $this->ticket->fields['priority'])))
        );

        $this->buildRow();

        $this->buildRow(format("Opening date", sprintf(
            __('%1$s by %2$s.', 'ticketexporter'),
            Html::convDateTime($this->ticket->fields['date']),
            $this->ticketRequesterName)));

        $this->buildRow(format("Last update", sprintf(
            __('%1$s by %2$s.', 'ticketexporter'),
            Html::convDateTime($this->ticket->fields['date_mod']),
            $this->getUserName(
                $this->ticket->fields['users_id_lastupdater']))));

        $this->buildRow();

        foreach ($this->getItilActors() as $type => $kind) {
            foreach ($kind as $data) {
                $this->buildRow(format(
                    $data['name'], $data['members'], false));
            }
            $this->buildRow();
        }

        foreach ($this->getLinkedTickets() as $link => $data) {
            $this->buildRow(format(
                $data['name'], $data['members'], false));
        }

        $this->buildRow();

        $this->buildRow(
            format("Time to own", Html::convDateTime(
                $this->ticket->fields['time_to_own'])),
            format("Time to resolve", Html::convDateTime(
                $this->ticket->fields['time_to_resolve']))
        );

        $this->buildRow(
            format("Internal time to own", Html::convDateTime(
                $this->ticket->fields['internal_time_to_own'])),
            format("Internal time to resolve", Html::convDateTime(
                $this->ticket->fields['internal_time_to_resolve']))
        );

        #if (!$pdfOnly && empty($this->ticketAttachments)) {
        if (empty($this->ticketAttachments)) {

            $this->buildRow();
            $this->buildRow(format(
                __("Associated document", 'ticketexporter'),
                __("None", 'ticketexporter')
            ));
        }

        if ($this->isChecked('followup') && empty($this->ticketFollowup)) {

            $this->buildRow();
            $this->buildRow(format(
                "Followup", __("No item to display")));
        }

        $this->buildTable();

        #if (!$pdfOnly && !empty($this->ticketAttachments)) {
        if (!empty($this->ticketAttachments)) {

            foreach ($this->ticketAttachments as $attachment => $data) {
                $this->tableContent .= '<tr align="center">';
                $this->tableContent .= '<td>' .
                    Html::clean($data['name']) . '</td>';
                $this->tableContent .= '<td>' .
                    Html::convDateTime($data['date_mod']) . '</td>';
                $this->tableContent .= '<td>' .
                    $this->getUserName($data['users_id']) . '</td>';
                $this->tableContent .= '<td>' . $data['mime'] . '</td>';
                $this->tableContent .= '<td>' . $data['filename'] . '</td>';
                $this->tableContent .= '</tr>';
            }

            $this->buildTable([
                'title' => _n("Associated document", "Associated documents",
                count($this->ticketAttachments), 'ticketexporter'),
                'columns' => [
                    __("Name"),
                    __("Date"),
                    sprintf(__("Added by %s"), ""),
                    __("MIME type"),
                    __("File")
                ]
            ]);
        }

        if ($this->isChecked('followup') && !empty($this->ticketFollowup)) {

            foreach ($this->ticketFollowup as $followup => $data) {
                $this->tableContent .= '<tr align="center">';
                $requestType = "";
                if ($data['requesttypes_id']) {
                    $requestType = __(Dropdown::getDropdownName(
                        'glpi_requesttypes', $data['requesttypes_id']));
                }
                $this->tableContent .= '<td>' . $requestType . '</td>';
                $this->tableContent .= '<td>' .
                    Html::convDateTime($data['date']) . '</td>';
                $this->tableContent .= '<td>' .
                    Html::clean($data['content']) . '</td>';
                $this->tableContent .= '<td>' .
                    $this->getUserName($data['users_id']) . '</td>';
                $this->tableContent .= '</tr>';
            }

            $this->buildTable([
                'title' => __("Followup"),
                'columns' => [
                    __("Source of followup"),
                    __("Date"),
                    __("Content"),
                    __("User")
                ]
            ]);
        }

        if (!empty($this->ticketApprovals)) {

            foreach ($this->ticketApprovals as $approval => $data) {
                $this->tableContent .= '<tr align="center">';
                $this->tableContent .= '<td>' .
                    TicketValidation::getStatus($data['status']) . '</td>';
                $this->tableContent .= '<td>' .
                    Html::convDateTime($data['submission_date']) . '</td>';
                $this->tableContent .= '<td>' .
                    $this->getUserName($data['users_id']) . '</td>';
                $this->tableContent .= '<td>' .
                    Html::convDateTime($data['validation_date']) . '</td>';
                $this->tableContent .= '<td>' .
                    $this->getUserName($data['users_id_validate']) . '</td>';
                $this->tableContent .= '</tr>';
            }

            $this->buildTable([
                'title' => __("Approval"),
                'columns' => [
                    __("Status"),
                    __("Request date"),
                    __("Approval requester"),
                    __("Approval date"),
                    __("Approver")
                ]
            ]);
        }

        return $this->output($pdfOnly);
    }


    /**
     * Returns a “user option” state.
     *
     * @param $option string
     *
     * @return boolean
    **/
    private function isChecked($option) {

        return (isset($this->userOptions[$option]) &&
            $this->userOptions[$option]);
    }


    /**
     * Returns a “printable” list of items.
     *
     * @param $items array
     *
     * @return array
    **/
    private function getPrintableList($items) {

        if (empty($items)) {
            return $items;
        }

        return implode(" / ", $items);
    }


    /**
     * Returns linked tickets (if any).
     *
     * @return array
    **/
    private function getLinkedTickets() {

        $results = Ticket_Ticket::getLinkedTicketsTo($this->ticketID);

        $linkedTickets = [
            Ticket_Ticket::LINK_TO => [
                'name' => Ticket_Ticket::getLinkName(Ticket_Ticket::LINK_TO),
                'data' => [],
                'members' => __("None", 'ticketexporter')
            ],
            Ticket_Ticket::DUPLICATE_WITH => [
                'name' => Ticket_Ticket::getLinkName(Ticket_Ticket::DUPLICATE_WITH),
                'data' => [],
                'members' => __("None", 'ticketexporter')
            ],
            Ticket_Ticket::SON_OF => [
                'name' => Ticket_Ticket::getLinkName(Ticket_Ticket::SON_OF),
                'data' => [],
                'members' => __("None", 'ticketexporter')
            ],
            Ticket_Ticket::PARENT_OF => [
                'name' => Ticket_Ticket::getLinkName(Ticket_Ticket::PARENT_OF),
                'data' => [],
                'members' => __("None", 'ticketexporter')
            ],
        ];

        if (is_array($results) && count($results)) {

            global $CFG_GLPI;

            $ticket = new Ticket();

            foreach ($results as $key => $data) {

                if ($ticket->getFromDB($data['tickets_id']) &&
                        array_key_exists($data['link'], $linkedTickets)) {

                    $linkedTickets[$data['link']]['data'][] = sprintf(
                        '<a href="%s%s">%s</a>',
                        $CFG_GLPI['url_base'],
                        $ticket->getFormURLWithID($data['tickets_id']),
                        Html::resume_text($ticket->fields['name'], 30)
                    );
                }
            }
        }

        foreach ($linkedTickets as $key => $data) {

            if (!empty($data['data'])) {
                $linkedTickets[$key]['members'] =
                    $this->getPrintableList($data['data']);
            }

            unset($linkedTickets[$key]['data']);
        }

        return $linkedTickets;
    }


    /**
     * Returns ITIL actors.
     *
     * @return array
    **/
    private function getItilActors() {

        $itilActors = [];

        $type = CommonITILActor::REQUESTER;

        $actors = $this->ticket->getUsers($type);
        $itilActors[$type]['users']['data'] = $actors;
        $itilActors[$type]['users']['name'] =
            _n("Requester", "Requesters", count($actors));
        $itilActors[$type]['users']['members'] = __("None", 'ticketexporter');

        $actors = $this->ticket->getGroups($type);
        $itilActors[$type]['groups']['data'] = $actors;
        $itilActors[$type]['groups']['name'] =
            _n("Requester group", "Requester groups", count($actors));
        $itilActors[$type]['groups']['members'] = __("None", 'ticketexporter');


        $type = CommonITILActor::OBSERVER;

        $actors = $this->ticket->getUsers($type);
        $itilActors[$type]['users']['data'] = $actors;
        $itilActors[$type]['users']['name'] =
            _n("Watcher", "Watchers", count($actors));
        $itilActors[$type]['users']['members'] = __("None", 'ticketexporter');

        $actors = $this->ticket->getGroups($type);
        $itilActors[$type]['groups']['data'] = $actors;
        $itilActors[$type]['groups']['name'] =
            _n("Watcher group", "Watcher groups", count($actors));
        $itilActors[$type]['groups']['members'] = __("None", 'ticketexporter');


        $type = CommonITILActor::ASSIGN;

        $actors = $this->ticket->getUsers($type);
        $itilActors[$type]['users']['data'] = $actors;
        $itilActors[$type]['users']['name'] = __("Assigned to");
        $itilActors[$type]['users']['members'] = __("Nobody", 'ticketexporter');

        $actors = $this->ticket->getGroups($type);
        $itilActors[$type]['groups']['data'] = $actors;
        $itilActors[$type]['groups']['name'] =
            _n("Assigned to group", "Assigned to groups",
                count($actors), 'ticketexporter');
        $itilActors[$type]['groups']['members'] = __("None", 'ticketexporter');

        $actors = $this->ticket->getSuppliers($type);
        $itilActors[$type]['suppliers']['data'] = $actors;
        $itilActors[$type]['suppliers']['name'] =
            _n("Assigned to supplier", "Assigned to suppliers",
                count($actors), 'ticketexporter');
        $itilActors[$type]['suppliers']['members'] = __("None", 'ticketexporter');


        foreach ($itilActors as $type => $data) {

            foreach ($data as $kind => $actors) {

                if (!empty($actors['data'])) {

                    $members = [];

                    switch ($kind) {

                        case 'users':
                            foreach ($actors['data'] as $actor) {
                                $members[] = $this->getUserName(
                                    $actor['users_id']);
                            }
                            break;

                        case 'groups':
                            foreach ($actors['data'] as $actor) {
                                $members[] = Dropdown::getDropdownName(
                                    'glpi_groups', $actor['groups_id']);
                            }
                            break;

                        case 'suppliers':
                            foreach ($actors['data'] as $actor) {
                                $members[] = Dropdown::getDropdownName(
                                    'glpi_suppliers', $actor['suppliers_id']);
                            }
                    }

                    $itilActors[$type][$kind]['members'] =
                        $this->getPrintableList($members);
                }

                unset($itilActors[$type][$kind]['data']);
            }
        }

        return $itilActors;
    }


    /**
     * Returns user name (& email if available).
     *
     * @param $userID string
     *
     * @return array
    **/
    private function getUserName($userID) {

        $user = new User();
        $user->getFromDB($userID);

        if ($_SESSION['glpinames_format'] == User::FIRSTNAME_BEFORE) {

            $userName = sprintf("%s %s",
                trim($user->fields['firstname']),
                trim($user->fields['realname'])
            );

        } else {

            $userName = sprintf("%s %s",
                trim($user->fields['realname']),
                trim($user->fields['firstname'])
            );
        }

        $userName = Html::clean($userName);

        if ($userName == "") {
            $userName = trim($user->fields['name']);
        }

        if ($user->fields['is_deleted']) {

            $userName = '<span style="color:#dd0000;">' . $userName . '</span>';

        } else {

            $email = UserEmail::getDefaultForUser($userID);

            if (!empty($email)) {

                $userName = '<a href="' . htmlspecialchars(
                    'mailto:' . rawurlencode($email) .
                    '?subject=' . rawurlencode($this->ticketFileName)
                ) . '">' . $userName . '</a>';
            }
        }

        return $userName;
    }


    /**
     * Returns followup flow (if any).
     *
     * @return boolean|array
    **/
    private function getFollowup() {

        if (!$this->isChecked('followup')) {
            return false;
        }

        $queryFilter = "";

        if (!Session::haveRight('followup', TicketFollowup::SEEPRIVATE)) {

            $queryFilter = "AND (is_private = 0 OR users_id = " .
                Session::getLoginUserID() . ")";
        }

        $query = "
            SELECT   requesttypes_id,
                     date,
                     content,
                     users_id
            FROM     glpi_ticketfollowups
            WHERE    tickets_id = $this->ticketID " . $queryFilter . "
            ORDER BY date DESC;
        ";

        return $this->fetchData($query);
    }


    /**
     * Returns approval flow (if any).
     *
     * @return boolean|array
    **/
    private function getApprovals() {

        if (!$this->isChecked('approval')) {
            return false;
        }

        $query = "
            SELECT   status,
                     submission_date,
                     users_id,
                     validation_date,
                     users_id_validate
            FROM     glpi_ticketvalidations
            WHERE    tickets_id = $this->ticketID
            ORDER BY submission_date DESC;
        ";

        return $this->fetchData($query);
    }


    /**
     * Returns ticket's associated documents (if any).
     *
     * @return array
    **/
    private function getAttachments() {

        $query = "
            SELECT     t2.name,
                       t2.filepath,
                       t2.filename,
                       t2.mime,
                       t2.date_mod,
                       t2.users_id
            FROM       glpi_documents_items t1
            INNER JOIN glpi_documents t2
                ON     t1.documents_id = t2.id
                AND    t2.is_blacklisted = 0
                AND    t2.is_deleted = 0
            WHERE      t1.items_id = $this->ticketID
                AND    t1.itemtype = 'Ticket';
        ";

        return $this->fetchData($query);
    }


    /**
     * Collects data from DB.
     *
     * @param $query string
     *
     * @return array
    **/
    private function fetchData($query) {

        global $DB;

        $data = [];

        if ($result = $DB->query($query)) {
            if ($DB->numrows($result)) {
                while ($item = $DB->fetch_assoc($result)) {
                    $data[] = $item;
                }
            }
        }

        return $data;
    }


    /**
     * Builds a two columns table row filled with given content.
     *
     * @param $left  string
     * @param $right string
     *
    **/
    private function buildRow($left = '', $right = '') {

        if (empty($right) && empty($left)) {

            $this->tableContent .=
                '<tr style="line-height: .6;"><td colspan="2"></td></tr>';

        } else if (!empty($right)) {

            $this->tableContent .=
                '<tr><td>' . $left . '</td><td>' . $right . '</td></tr>';

        } else {

            $this->tableContent .= '<tr><td colspan="2">' . $left . '</td></tr>';
        }

    }


    /**
     * Builds HTML table markup.
     *
     * @param $headerData array
     *
    **/
    private function buildTable($headerData = []) {

        $tableFooter = '</table>';

        if (empty($headerData)) {

            $tableHeader =
                '<table border="0" cellspacing="0" cellpadding="0">';

        } else {

            $tableHeader =
                '<table border="1" cellspacing="0" cellpadding="3">';

            if (isset($headerData['columns']) &&
                    !empty($headerData['columns'])) {

                if (isset($headerData['title']) &&
                        !empty($headerData['title'])) {

                    $tableHeader = '<br><h4>' . $headerData['title'] .
                        '</h4>' . $tableHeader;
                }

                $tableLegend = '<tr border="1" align="center">';

                foreach ($headerData['columns'] as $colTitle) {
                    $tableLegend .= '<th><i>' . $colTitle . '</i></th>';
                }

                $tableLegend .= '</tr>';

                $tableHeader .= $tableLegend;
                $tableFooter = $tableLegend . $tableFooter;

            }
        }

        $this->html .= $tableHeader . $this->tableContent . $tableFooter;
        $this->tableContent = '';

    }


    /**
     * Renders PDF inline / as a string.
     *
     * @param $pdfOnly boolean
     *
     * @return string
    **/
    private function output($pdfOnly = false) {

        $this->pdf->AddPage();
        $this->pdf->writeHTML($this->html);

        if ($pdfOnly) {

            return ($this->pdf->Output(
                $this->transferFileName . ".pdf", 'I') === '');

        } else {

             return $this->pdf->Output("", 'S');
        }

    }

}

// EOF
