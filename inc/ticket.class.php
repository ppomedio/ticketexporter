<?php

/**
 * Plugin's ticket class.
 *
**/
class PluginTicketexporterTicket extends CommonDBTM {


    /**
     * Defines ticket tab title.
     *
     * @param $item         CommonGLPI object
     * @param $withtemplate boolean
     *
     * @return string
    **/
    public function getTabNameForItem(
            CommonGLPI $item,
            $withtemplate = 0
        ) {

        return __("Exporting", 'ticketexporter');
    }


    /**
     * Defines export tab content.
     *
     * @param $item         CommonGLPI object
     * @param $tabnum       integer
     * @param $withtemplate boolean
     *
     * @return boolean
    **/
    public static function displayTabContentForItem(
            CommonGLPI $item,
            $tabnum = 1,
            $withtemplate = 0
        ) {

        $formCssId = uniqid();

        $html = '<h2 id="ticket_export-title-' . $formCssId .
            '" class="header">';
        $html .= __("Exporting", 'ticketexporter');
        $html .= '</h2><br/>';
        $html .= '<form id="ticket_export-form-' . $formCssId . '" action="' .
            self::getFormURL() . '" method="GET">';

        $html .= Html::getCheckbox([
            'id' => "ticket_export-form-pdf_only-" . $formCssId,
            'name' => "pdf_only",
            'checked' => true
        ]);
        $html .= '&nbsp;<label for="ticket_export-form-pdf_only-' .
            $formCssId . '">';
        $html .= __("PDF only (no attachments)", 'ticketexporter') .
            '</label><br/><br/>';

        if (Session::haveRightsOr(
                'ticketvalidation', TicketValidation::getValidateRights())) {

            $html .= Html::getCheckbox([
                'id' => "ticket_export-form-approval-" . $formCssId,
                'name' => "approval",
                'checked' => false
            ]);
            $html .= '&nbsp;<label for="ticket_export-form-approval-' .
                $formCssId . '">';
            $html .= __("Approval") . '</label><br/><br/>';
        }

        $html .= Html::getCheckbox([
            'id' => "ticket_export-form-followup-" . $formCssId,
            'name' => "followup",
            'checked' => false
        ]);
        $html .= '&nbsp;<label for="ticket_export-form-followup-' .
            $formCssId . '">';
        $html .= __("Followup") . '</label><br/><br/>';

        $html .= Html::hidden("id", ['value' => $item->getField('id')]);
        $html .= Html::submit(__("Export ticket", 'ticketexporter'));
        $html .= '</form>';

        echo $html;

        echo Html::scriptBlock('
$("#ticket_export-form-' . $formCssId . '").on("submit", function() {
    $header = $("#ticket_export-title-' . $formCssId . '");
    $(this).fadeOut("fast", function() {
        $header.text("' .
            __("Ticket export in progress…", 'ticketexporter') . '");
    }).delay(1000).fadeIn("fast", function() {
        $header.text("' . __("Exporting", 'ticketexporter') . '");
    });
});
        ');

        return true;
    }


    /**
     * Performs ticket exportation.
     *
     * @param $formData array
     *
     * @return boolean
    **/
    public static function export($formData) {

        $tools = new PluginTicketexporterToolbox();
        $export = new PluginTicketexporterExport();

        if (!$export->init($formData)) {
            return false;
        };

        if (isset($formData['pdf_only']) && $formData['pdf_only']) {
            return $export->createPdf(true);
        }

        $archivePathname = $tools->createTempPathname(
            $tools->getWorkingFolder());
        $ticketFileName = $export->ticketFileName;

        $archiveAttachments = [];
        $data = $export->ticketAttachments;
        foreach ($data as $item) {
            $archiveAttachments[] = [
                'filepath' => GLPI_DOC_DIR .
                    DIRECTORY_SEPARATOR . $item['filepath'],
                'filename' => sprintf("%s/%s/%s",
                    $ticketFileName,
                    _n("Associated document", "Associated documents",
                        count($data), 'ticketexporter'),
                    $item['filename']
                )
            ];
        }

        if ($tools->createArchive(
                $archivePathname,
                sprintf("%s/%s.pdf", $ticketFileName, $ticketFileName),
                $export->createPdf(),
                $archiveAttachments
            )) {

            if ($tools->sendArchive(
                    $archivePathname,
                    $export->transferFileName
                )) {

                return unlink($archivePathname);
            }
        }

        return false;
    }

}

// EOF
