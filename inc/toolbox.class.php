<?php

/**
 * Plugin tools class.
 *
**/
class PluginTicketexporterToolbox {

    private $workingFolder;


    /**
     * Class initialization.
     *
    **/
    public function __construct() {

        $this->workingFolder = GLPI_PLUGIN_DOC_DIR .
            DIRECTORY_SEPARATOR .
            "_ticketexporter";

    }


    /**
     * Creates working folder.
     *
     * @return boolean
    **/
    public function createWorkingFolder() {

        if (!file_exists($this->workingFolder)) {
            return mkdir($this->workingFolder, 0755, false);
        } else {
            return true;
        }

    }


    /**
     * Returns working folder pathname.
     *
     * @return string
    **/
    public function getWorkingFolder() {

        return $this->workingFolder;
    }


    /**
     * Creates local temporary pathname.
     *
     * @param $folderPath string
     *
     * @return string
    **/
    public function createTempPathname($folderPath) {

        return $folderPath .
            DIRECTORY_SEPARATOR .
            uniqid('', false);
    }


    /**
     * Removes working folder.
     *
     * @param $pathname string
     *
     * @return boolean
    **/
    public function removeWorkingFolder() {

        #Toolbox::deleteDir($this->workingFolder);
        $this->destroyFolder($this->workingFolder);

    }


    /**
     * Creates ZIP archive.
     *
     * @param $pathname    string
     * @param $pdfFileName string
     * @param $pdfContent  string
     * @param $attachments array
     *
     * @return boolean
    **/
    public function createArchive(
            $pathname = "",
            $pdfFileName = "",
            $pdfContent = "",
            $attachments = []
        ) {

        if (empty($pathname) || empty($pdfFileName) || empty($pdfContent)) {
            return false;
        }

        $zip = new ZipArchive();

        if ($zip->open($pathname, ZipArchive::CREATE) !== true) {
            return false;
        }

        if ($zip->addFromString($pdfFileName, $pdfContent) !== true) {
            return false;
        }

        foreach ($attachments as $item) {
            $zip->addFile($item['filepath'], $item['filename']);
        }

        if ($zip->close()) {
            return file_exists($pathname);
        }

        return false;
    }


    /**
     * Performs archive download.
     *
     * @param $pathname string
     * @param $fileName string
     *
     * @return boolean
    **/
    public function sendArchive($pathname = "", $fileName = "") {

        if (!file_exists($pathname) || empty($fileName)) {
            return false;
        }

        header('Content-Type: application/zip');
        header('Cache-Control: no-store, max-age=0');
        header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        header('Content-Disposition: attachment; filename="' . $fileName . '.zip"');
        header('Content-Length: ' . filesize($pathname));
        readfile($pathname);

        return true;
    }


    /**
     * Recursively deletes an existing folder and its content.
     *
     * @param $pathname string
     *
    **/
    private function destroyFolder($pathname) {

        if (file_exists($pathname)) {

            foreach (glob($pathname . '*', GLOB_MARK) as $item) {

                if (is_dir($item)) {

                    self::destroyFolder($item);
                    rmdir($item);

                } else {

                    unlink($item);
                }
            }
        }

    }

}

// EOF
