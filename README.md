## TicketExporter GLPI plugin

A plugin to export (in PDF format) a GLPI (v9.3.3) ticket and all related documents as an archive file.

## Contributing

* Open a ticket for each bug/feature so it can be discussed.
* Follow [development guidelines](http://glpi-developer-documentation.readthedocs.io/en/latest/plugins/index.html).
* Refer to [GitFlow](http://git-flow.readthedocs.io/) process for branching.
* Work on a new branch on your own fork.
* Open a PR that will be reviewed by a developer.
