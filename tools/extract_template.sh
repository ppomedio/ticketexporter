#!/bin/bash
###
# This file is a modified version of the one coming with
# the GLPI Tools that does not work under Mac OS.
#
# If it does not work for you, try the original one
# found in “../vendor/bin”.
#
###

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd $DIR > /dev/null

NAME="ticketexporter"
POTFILE=$NAME.pot

if [ $(basename $(pwd)) = "tools" ]; then
    cd ..
fi

if [ ! $(basename $(pwd)) = $NAME ]; then
    echo "Please run this script either from plugin root or from its “tools” folder."
    exit 0
fi

PHP_SOURCES=`find ./ -name \*.php -not -path "./vendor/*"`

if [ ! -d "locales" ]; then
    mkdir locales
fi

xgettext $PHP_SOURCES -o locales/$POTFILE -L PHP --add-comments=TRANS --from-code=UTF-8 --force-po \
    --keyword=_n:1,2,4t --keyword=__s:1,2t --keyword=__:1,2t --keyword=_e:1,2t --keyword=_x:1c,2,3t --keyword=_ex:1c,2,3t \
    --keyword=_sx:1c,2,3t --keyword=_nx:1c,2,3,5t

LANG=C msginit --no-translator -i locales/$POTFILE -l en_GB -o locales/en_GB.po

echo "Created locales/"$POTFILE"."

popd > /dev/null
