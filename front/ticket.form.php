<?php

/**
 * Plugin's ticket form action.
 *
**/

include_once (join(DIRECTORY_SEPARATOR, [
    "..", "..", "..", "inc/includes.php"
]));

Session::checkLoginUser();

if (isset($_GET['id']) && is_numeric($_GET['id'])) {

    if (PluginTicketexporterTicket::export($_GET)) {

        exit(0);
    }
}

Session::addMessageAfterRedirect(
    __("An error occurred during export.", 'ticketexporter'),
    true,
    ERROR
);

Html::redirect(Ticket::getSearchURL());

// EOF
