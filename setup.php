<?php

define('PLUGIN_TICKETEXPORTER_VERSION', '1.0.1');


/**
 * Plugin init.
 *
 * @return void
 */
function plugin_init_ticketexporter() {

    global $PLUGIN_HOOKS;

    $PLUGIN_HOOKS['csrf_compliant']['ticketexporter'] = true;
    Plugin::registerClass('PluginTicketexporterTicket', [
        'addtabon' => 'Ticket'
    ]);
}


/**
 * Provides plugin name, version etc.
 *
 * @return array
 */
function plugin_version_ticketexporter() {

    return [
        'name'         => __("Ticket Exporting", 'ticketexporter'),
        'version'      => PLUGIN_TICKETEXPORTER_VERSION,
        'author'       => "Philippe Pomédio",
        'license'      => "CeCILL-C",
        'homepage'     => "",
        'requirements' => [
            'glpi' => [
                'min' => "9.3",
                'max' => "9.4",
            ],
            'php' => [
                'min' => "5.6",
            ]
        ]
    ];
}


/**
 * Pre-requisites check before install.
 *
 * @return boolean
 */
function plugin_ticketexporter_check_prerequisites() {

    // Needed only if plugin may be used on GLPI < v9.2.

    if (version_compare(GLPI_VERSION, '9.3', '<')
            || version_compare(GLPI_VERSION, '9.4', '>=')) {

        echo  __("This plugin requires GLPI v9.3.", 'ticketexporter');
        return false;
    }

    return true;
}


/**
 * Configuration check.
 *
 * @param $verbose   boolean
 *                   Whether to display message on failure.
 *
 * @return boolean
 */
function plugin_ticketexporter_check_config($verbose = false) {

    if (true) { // Check configuration here…
        return true;
    }

    if ($verbose) {
        echo __("Configuration check failure.", 'ticketexporter');
    }

    return false;
}

// EOF
